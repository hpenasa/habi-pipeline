import boto3
import os


bucket_name = os.environ.get("S3_BUCKET")
file_name = 'habi.png'
print(bucket_name)

def lambda_handler(event, context):
    s3 = boto3.client('s3')
    key_name = file_name
    s3.upload_file(file_name, bucket_name, key_name)

    return {
            'status': 'True',
       'statusCode': 200,
       'body': 'Image Uploaded'
      }


print(lambda_handler("", ""))
